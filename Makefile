OPTIPNG =	optipng -o7 -zm1-9 -clobber

.PHONY: all img clean strip-svg optipng

all: optipng img strip-svg

.SUFFIXES: .svg .png
.svg.png:
	inkscape --export-type=png -d 300 $<

clean:
	rm -f *.eps *.pdf *.aux	rm -f *.log *.aut *.tex *.toc
	rm -f *.dvi *.out img/*.eps
	rm -rf img

img:
	mkdir -p img
	cp -R raw/*.svg raw/*.png img

strip-svg: img
	for svg in img/*.svg; do				\
		inkscape --export-type=png -d 300 $$svg;	\
		${OPTIPNG} $${svg%%.svg}.png;			\
	done

optipng:
	for png in raw/*.png; do		\
		${OPTIPNG} $$png;		\
	done
