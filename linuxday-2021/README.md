# LinuxDay 2021

Sorgenti e PDF della presentazione tenuta il 23 ottobre 2021 al Linux
Day italiano (online.)

I sorgenti LaTeX (`pr.tex`) e gli screenshot di ariane sono © Omar Polo
e rilasciati con licenza CC0 1.0 Universal (CC0 1.0) Public Domain
Dedication: https://creativecommons.org/publicdomain/zero/1.0/

Le immagini dell'hypertesto sono © Gnuserland e, similmente, rilasciati
con licenza CC0 1.0 Universal.

I loghi del Linux Day sono copyright dell'organizzazione del linux day.
