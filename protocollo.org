#+TITLE: Gemini
#+LANGUAGE: it
#+BEAMER_HEADER: \subtitle{introduzione allo standard (provvisorio)}
#+AUTHOR: Gemini Italia (https://codeberg.org/gemini-italia)
#+options: H:1
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS:
#+columns: %45ITEM %10BEAMER_env(Env) %10BEAMER_act(Act) %4BEAMER_col(Col) %8BEAMER_opt(Opt)
#+LaTeX_header: \usepackage[it]{babel}
#+beamer_theme: metropolis
#+beamer_color_theme: seahorse
#+beamer_font_theme:
#+beamer_inner_theme:
#+beamer_outer_theme:
#+beamer_header: \definecolor{default-color}{rgb}{0.16796875, 0.1796875, 0.1796875}
#+beamer_header: \setbeamercolor{palette primary}{fg=default-color}
#+beamer_header: \setbeamercolor{palette secondary}{fg=default-color}
#+beamer_header: \setbeamercolor{palette tertiary}{fg=default-color}
#+beamer_header: \setbeamercolor{palette quaternary}{fg=default-color}
#+beamer_header: \setbeamercolor{structure}{fg=default-color}
#+beamer_header: \setbeamercolor{section in toc}{fg=default-color}
#+startup: beamer
#+BEAMER_FRAME_LEVEL: 2

* Definizioni

#+ATTR_BEAMER: :overlay +-
- Gemini è:
  - una delle costellazioni dello zodiaco;

  - un progetto areospaziale della NASA (tra /Mercury/ ed /Apollo/);
    #+begin_quote
      \tiny https://en.wikipedia.org/wiki/Project_Gemini
    #+end_quote

  - un protocollo internet innovativo!

* Cos'è Gemini?

Un insieme di documenti che definiscono:

- protocollo di rete di livello 7, o ``livello dell'applicazione'';

- un  formato  di  file  per ipertesti (*gemtext*).

* Ipertesto

  - Un documento che contiene riferimenti (facilmente consultabili) ad
    altre risorse.

  #+ATTR_LATEX: :height 6cm
  [[./img/ipertesto.png]]


* Ipertesto

  - il protocollo e il gemtext  usano entrambi le URI per identificare
    le risorse  di rete quindi  sono possibili collegamenti  che usano
    schemi differenti.

  #+ATTR_LATEX: :height 6cm
  [[./img/ipertesto-verso-www.png]]

* Il gemtext

#+ATTR_BEAMER: :overlay +-
  - Quasi un sottoinsieme di Markdown od orgmode;
  - Pochi elementi a disposizione:

    1. tre livelli d'intestazione
       - \large Livello 1
       - Livello 2
       - \tiny   Livello 3

    2. liste non numerate

    3. citazioni

    4. link

    5. blocchi di testo preformattato

      #+ATTR_LATEX: :width 5cm
      [[./img/preformatted-text.png]]

    6. linee di testo (paragrafi)

* Il gemtext

** Esempio
   \tiny
#+BEGIN_SRC text
  ``` logo
      Il gemlog di ...
  ```

  # Il protocollo Gemini in italiano
  ## Contenuti

  Per il momento questi sono i documenti che abbiamo scritto.
  Non esiste ancora una traduzione della documentazione ufficiale di Gemini.
  ^^^ NB: in origine questo testo era sulla stessa linea

  ## Fonte

  Queste pagine sono pubblicate a partire dal repository condiviso ospitato su Codeberg

  => https://codeberg.org/gemini-italia/documentazione

  Chiunque può:

  ,* clonare il repository;
  ,* pubblicare questa documentazione sulla propria capsula.

  > Inutile lo sono sempre stato, inutile sì, ma libero!!
#+END_SRC

* Il gemtext

** Intestazioni
   \tiny
#+BEGIN_SRC text
  # Il protocollo Gemini in italiano
  ## ...
  ###  ...
#+END_SRC

Allo stato attuale lo spazio dopo l'ultimo cancelletto *non è necessario* ma questa regola potrebbe cambiare in futuro.

#+BEGIN_SRC text
  heading     = '#'   SP text end-of-line   ;  ← notare SP
                   / '##'  text end-of-line
                   / '###' text end-of-line
  SP          =  from RFC-5234              ; ← %x20
  end-of-line = [\r]\n                      ; il \r fa parte del terminatore di linea

  https://gitlab.com/gemini-specification/gemini-text/-/issues/7
#+END_SRC

* Il gemtext

** Liste non numerate
   \tiny
#+BEGIN_SRC text
  * clonare il repository;
  * pubblicare questa documentazione sulla propria capsula.
#+END_SRC

Lo spazio dopo l'asterisco  *è necessario*.

#+BEGIN_SRC text
  * Lo spazio dopo l'asterisco  *è necessario*. ; ← per distinguere dall'evidenziazione del testo
#+END_SRC

* Il gemtext

** Citazioni
   \tiny
#+BEGIN_SRC text
  > Inutile lo sono sempre stato, inutile sì, ma libero!!
#+END_SRC

Lo spazio dopo il segno maggiore *non è obbligatorio* ma chissà ;-)

* Il gemtext
\tiny
** Link
  I link spezzano il flusso del testo → niente link inline

#+BEGIN_SRC text
  => gemini://gemini.circumlunar.space/ sito ufficiale
     ^^^ URI                            ^^^ etichetta
#+END_SRC

- Lo spazio dopo la stringa ~'=>'~ non è obbligatorio;
- l'etichetta non e' obbligatoria

#+BEGIN_SRC text
  => altri/documenti
     ^^^ si assume lo schema gemini
  => https://codeberg.org/gemini-italia
    ^ altri schemi sono ammessi
  =>https://codeberg.org/gemini-italia
    ^ niente spazio
  =>       immagini/gnu.png
    ^^^^^^^ in teoria solo uno spazio è previsto
#+END_SRC

* Il gemtext

** Blocchi di testo preformattato
\tiny
#+BEGIN_SRC text
  ``` logo ←----------- testo alternativo
    ^^^ interruttore
    _     ___   ____  ___
   | |   / _ \ / ___|/ _ \
   | |  | | | | |  _| | | |
   | |__| |_| | |_| | |_| |
   |_____\___/ \____|\___/


  ```
  ^^^ interruttore
#+end_src

\tiny
- la stringa ~```~ agisce da interruttore di cambio stato tra "modalità testo preformattato" e "modalità testo *non* preformattato"
  - \tiny questa regola genera la necessità di mantenere uno stato per il parser
- il testo alternativo è opzionale, ma se usate l'ASCIIart è meglio usarlo (migliora l'accessibilità);

* Il gemtext

** Paragrafi

Tutto il resto

Non viene specificata la presentazione del contenuto

* Il protocollo di comunicazione
#+ATTR_BEAMER: :overlay +-
- Obbligatorio l'uso del TLS;
  - utilizzo dei certificati per autenticare sia il server che il client.
- metadati ridotti al minimo indispensabile;
  - non è possibile stabilire quando un flusso dati sarà interrotto dal server;
    - il client deve assumere sempre un flusso infinito di dati, eccetto pochi casi convenzionalmente accettati (gemlog).

* Il protocollo di comunicazione

  - Si assume che un valido canale TLS sia stato instaurato
    #+ATTR_LATEX: :width 8cm
    [[./img/sequenza-richiesta.png]]


* Il protocollo di comunicazione

** richiesta
  - somiglia, grossolanamente, alla GET dell HTTP

    #+BEGIN_SRC text
      request := absolute-URI CRLF

      CRLF := \r\n
      Esempio:

      gemini://gemini.circumlunar.space:1965/a?a=1&b=2

    #+END_SRC
  - non può superare i 1024 otetti

* Il protocollo di comunicazione

** risposta
#+BEGIN_SRC text
  20 SP          text/gemini CRLF corpo della risposta
  ^^ due cifre   ^^^^^^^^^^^ tipo MIME
#+END_SRC

* Il protocollo di comunicazione

  - Famiglie di codici di risposta
    - pattern: codice spazio /meta/
    - 1x :: ci si aspetta un input (tramite la query dell'URI)
      Meta: il prompt per l'input (es: /quale carta giocare?/)
    - 20 :: la richiesta può essere soddisfatta
    - 3x :: redirezione
      - il massimo livello di redirezioni è 5
        (la redirezione *deve* essere accettata tramite esplicita azione dell'utente?)
        /giusta osservazione/
        Meta, l'URI al quale rimandare (relativa o assoluta)
    - 4x :: errore temporaneo nel reperire la risorsa, meta può essere un messaggio di errore comprensibile
    - 5x :: errore temporaneo nel reperire la risorsa, meta può essere un messaggio di errore comprensibile
    - 6x :: errori nell'autenticazione

* Il protocollo di comunicazione

  - I codici 6x
    - 60 :: è richiesto un certificato per accedere alla risorsa
      il client può
       1.  generarne uno
       2.  richiedere nuovamente la risorsa
    - 61 :: il certificato non è una credenziale valida per la risorsa
    - 62 :: il cerificato non è una credenziale valida in sè

* Il protocollo di comunicazione

** Autenticazione del server
   - Autenticazione del server
     - TOFU (Trust On First Use)
       - ha senso firmarlo da una CA?
       - quando scade?
       - cosa succede se scade?
       - cosa succede se il server lo cambia?

* Risorse

  - documentazione ufficiale (parzialmante tradotta in italiano)
    [[https://gemini.circumlunar.space/docs/it/]]

  - gruppo di lavoro per la definizione dello standard
    [[https://gitlab.com/gemini-specification]]

  - approfondimenti per chi vuole avvicinarsi a gemini
    [[https://geminiquickst.art/]]

* Contatti

  - IRC server: /irc.libera.chat/, canale /#gemini-it/
  - Fediverso: cage@libretux.com [e chi altri voglia aggiungersi ;-)]
  - Vuoi collaborare? [[https://codeberg.org/gemini-italia]]

* Riconoscimenti

#+LATEX: \tiny
#+BEGIN_SRC text

Per questo documento © 2021 cage rilasciato con licenza:
CC-BY-SA https://creativecommons.org/licenses/by-sa/4.0/deed.it

#+END_SRC
